package org.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    //starting variables
        int num;
        String str;
        int i=0;
        Scanner keyboard = new Scanner(System.in);

        //inform the user of what the program does
        System.out.println("This program enters values into a database.");
        System.out.println("There are 3 values to put in: ID number, First Name, Last Name.");

        //create the object to hold out data
        EmployeeEntity obj1 = new EmployeeEntity();

        while (i==0) { //beginning of the entry loop
            i=1;
            //ask the user for input
            while (i==1) { //beginning of the ID entry check
                try {
                    System.out.println("Please enter the ID number: ");
                    num = keyboard.nextInt();
                    obj1.setEmployId(num);
                    i=0; //break the loop
                    keyboard.nextLine(); //clear buffer
                } catch (InputMismatchException e) {
                    System.out.println("That is not a valid integer. Whole numbers only.");
                    keyboard.nextLine(); //clear line and repeat
                }
            }
            System.out.println("Please enter the FIRST name: ");
            str = keyboard.nextLine();
            obj1.setfName(str);

            System.out.println("Please enter the LAST name: ");
            str = keyboard.nextLine();
            obj1.setlName(str);
            i=2;

            while (i==2) {
                System.out.println("You entered " + obj1.getEmployId() + " " + obj1.getfName() +
                        " " + obj1.getlName() + ".");
                System.out.println("Is this correct?");
                System.out.println("Yes/No/Exit (y/n/e): ");
                str = keyboard.nextLine();
                switch (str) {
                    case "y": //continue
                    case "Y": //begin sending the data
                        //build a configuration setting
                        Configuration con = new Configuration().configure().addAnnotatedClass(EmployeeEntity.class);
                        //use the configuration to create a session
                        SessionFactory sf = con.buildSessionFactory();
                        //open the session for transmission
                        Session session = sf.openSession();
                        //begin transaction of information
                        Transaction tx = session.beginTransaction();
                        //save/send the data of obj1
                        session.save(obj1);
                        //commit information
                        tx.commit();
                        //break the loop
                        i=3;
                        break;
                    case "n": //continue
                    case "N": //restart the loop
                        System.out.println("Data not accurate. Restarting entry process...");
                        keyboard.nextLine();
                        i=0;
                        break;
                    case "e": //continue
                    case "E": //exit the program
                        System.out.println("Exit option chosen. Have a good day.");
                        System.exit(2);
                    default:
                        System.out.println("Invalid response, please enter y or n or e.");


                }
            }

        }
        System.out.println("End of program. Thank you for your time.");

    }
}
