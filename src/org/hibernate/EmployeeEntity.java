package org.hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "employee", schema = "hibernate", catalog = "")
public class EmployeeEntity {
    private int employId;
    private String lName;
    private String fName;

    @Id
    @Column(name = "EmployID")
    public int getEmployId() {
        return employId;
    }

    public void setEmployId(int employId) {
        this.employId = employId;
    }

    @Basic
    @Column(name = "LName")
    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    @Basic
    @Column(name = "FName")
    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeEntity that = (EmployeeEntity) o;
        return employId == that.employId && Objects.equals(lName, that.lName) && Objects.equals(fName, that.fName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employId, lName, fName);
    }
}
